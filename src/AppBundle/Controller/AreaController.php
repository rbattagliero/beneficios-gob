<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Area;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Area controller.
 *
 * @Route("area")
 */
class AreaController extends Controller
{
    /**
     * Lists all area entities.
     *
     * @Route("/", name="area_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $filter = array();
        $filter = $request->get('filter');
        
        $areas = $em->getRepository('AppBundle:Area')->getBenefitsByFilter($filter);

        return $this->render('AppBundle:area:index.html.twig', array(
            'areas' => $areas,
            'filter'   => $filter
        ));
    }

    /**
     * Creates a new area entity.
     *
     * @Route("/new", name="area_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $area = new Area();
        $form = $this->createForm('AppBundle\Form\AreaType', $area);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if(!$em->getRepository('AppBundle:Area')->findByName($area->getName())){
                $em->persist($area);
                $em->flush();
                $this->addFlash('success','Se agregó el área correctamente.');
                return $this->redirectToRoute('area_index');
            }else{
                $this->addFlash('error','El nombre ya existe.');
            }
        }

        return $this->render('AppBundle:area:new.html.twig', array(
            'area' => $area,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a area entity.
     *
     * @Route("/{id}", name="area_show")
     * @Method("GET")
     */
    public function showAction(Area $area)
    {
        $deleteForm = $this->createDeleteForm($area);

        return $this->render('AppBundle:area:show.html.twig', array(
            'area' => $area,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing area entity.
     *
     * @Route("/{id}/edit", name="area_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Area $area)
    {
        $deleteForm = $this->createDeleteForm($area);
        $editForm = $this->createForm('AppBundle\Form\AreaType', $area);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success','Se modificó el área correctamente.');
            return $this->redirectToRoute('area_index');
        }

        return $this->render('AppBundle:area:edit.html.twig', array(
            'area' => $area,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a area entity.
     *
     * @Route("/{id}/delete", name="area_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, Area $area)
    {
        try{
            $em = $this->getDoctrine()->getManager();
            $em->remove($area);
            $em->flush();

            $this->addFlash('success','Se eliminó correctamente el registro.');
            
        }catch(ForeignKeyConstraintViolationException $e){
            $this->addFlash('error','No se puede eliminar el registro porque tiene beneficiarios asociados. Elmine la relación e intente de nuevo.');
        }

        return $this->redirectToRoute('area_index');
    }

    /**
     * Creates a form to delete a area entity.
     *
     * @param Area $area The area entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Area $area)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('area_delete', array('id' => $area->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
