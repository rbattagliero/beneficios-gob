<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Benefit;
use AppBundle\Entity\FilePdf;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Benefit controller.
 *
 * @Route("benefit")
 */
class BenefitController extends Controller
{
    /**
     * Lists all benefit entities.
     *
     * @Route("/", name="benefit_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $filter = array();
        $filter = $request->get('filter');
        
        $benefits = $em->getRepository('AppBundle:Benefit')->getBenefitsByFilter($filter);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $benefits, 
            $request->query->getInt('page', 1),
            20
        );
        
        return $this->render('AppBundle:benefit:index.html.twig', array(
            'pagination' => $pagination,
            'types'    => Benefit::TYPE,
            'filter'   => $filter
        ));
    }

    /**
     * @Route("/benefit_pdf/{id}", name="benefit_pdf")
     * @Method("GET")
     */
    public function pdfAction(Benefit $benefit)
    {
        $em = $this->getDoctrine()->getManager();
        
        $filesNames = array(1=>'',2=>'',3=>'',4=>'',5=>'');
        foreach($benefit->getFilePdfs() as $file){
            $keys = explode('_', $file->getName());
            $filesNames[$keys[0]] = $file->getWebPath();
        }
        
        $namePDF = 'beneficiario_'.$benefit->getFullName().'_'.date('d-m-Y').'.pdf';
        
        $pdfGenerator = $this->get('spraed.pdf.generator');
        $html = $this->renderView('AppBundle:benefit:pdf.html.twig',array(
            'benefit' => $benefit,
            'files' => $filesNames
        ));
        $pdfGenerator->generatePDF($html, 'UTF-8');
        $pdfGenerator = $this->get('spraed.pdf.generator');

        return new Response($pdfGenerator->generatePDF($html),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attached; filename="'.$namePDF.'"'
            )
        );
        
    }

    /**
     * @Route("/benefit_list_pdf", name="benefit_list_pdf")
     * @Method("GET")
     */
    public function pdfListAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $filter = array();
        $filter = $request->get('filter');
        
        $benefits = $em->getRepository('AppBundle:Benefit')->getBenefitsByFilter($filter);

        $namePDF = 'listado_';
        $namePDF .= ($filter['type'] == 1) ? 'becas' : 'contratos';
        $namePDF .= '_'.date('d-m-Y') . '.pdf';
        $titlePDF = 'Listado de ';
        $titlePDF .= ($filter['type'] == 1) ? 'becas' : 'contratos';
        $titlePDF .= ' '. date('d-m-Y');

        $pdfGenerator = $this->get('spraed.pdf.generator');
        $html = $this->renderView('AppBundle:benefit:pdf_list.html.twig',array(
            'benefits' => $benefits->getResult(),
            'title' => $titlePDF
        ));
        $pdfGenerator->generatePDF($html, 'UTF-8');
        $pdfGenerator = $this->get('spraed.pdf.generator');

        return new Response($pdfGenerator->generatePDF($html),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attached; filename="'.$namePDF.'"'
            )
        );
    }

    /**
     * Lists all benefit entities.
     *
     * @Route("/contrato", name="contratos_index")
     * @Method("GET")
     */
    public function contratoAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $filter = array();
        $filter = $request->get('filter');
        $filter['type'] = 2;
        
        $benefits = $em->getRepository('AppBundle:Benefit')->getBenefitsByFilter($filter);

        $search['filter'] = array();
        $search['filter']['type'] = $filter['type'];
        if(isset($filter['name'])){
            $search['filter']['name'] = $filter['name'];
        }else{
            $search['filter']['name'] = '';
        }
        if(isset($filter['dni'])){
            $search['filter']['dni'] = $filter['dni'];
        }else{
            $search['filter']['dni'] = '';
        }
        if(isset($filter['type'])){
            $search['filter']['type'] = $filter['type'];
        }else{
            $search['filter']['type'] = '';
        }
        if(isset($filter['passport'])){
            $search['filter']['passport'] = $filter['passport'];
        }else{
            $search['filter']['passport'] = '';
        }
        if(isset($filter['cuil'])){
            $search['filter']['cuil'] = $filter['cuil'];
        }else{
            $search['filter']['cuil'] = '';
        }
        if(isset($filter['area'])){
            $search['filter']['area'] = $filter['area'];
        }else{
            $search['filter']['area'] = '';
        }
        if(isset($filter['ministry'])){
            $search['filter']['ministry'] = $filter['ministry'];
        }else{
            $search['filter']['ministry'] = '';
        }
        if(isset($filter['secretariat'])){
            $search['filter']['secretariat'] = $filter['secretariat'];
        }else{
            $search['filter']['secretariat'] = '';
        }
        if(isset($filter['direction'])){
            $search['filter']['direction'] = $filter['direction'];
        }else{
            $search['filter']['direction'] = '';
        }

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $benefits, 
            $request->query->getInt('page', 1),
            20
        );

        $areas = $em->getRepository('AppBundle:Area')->findAll();
        $ministries = $em->getRepository('AppBundle:Ministry')->findAll();
        $secretariats = $em->getRepository('AppBundle:Secretariat')->findAll();
        $directions = $em->getRepository('AppBundle:Direction')->findAll();
        
        return $this->render('AppBundle:benefit:contratos.html.twig', array(
            'pagination' => $pagination,
            'types'    => Benefit::TYPE,
            'filter'   => $filter,
            'search'   => $search,
            'areas'    => $areas,
            'ministries'    => $ministries,
            'secretariats' => $secretariats,
            'directions' => $directions,
        ));
    }

    /**
     * Lists all benefit entities.
     *
     * @Route("/becas", name="becas_index")
     * @Method("GET")
     */
    public function becasAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $filter = array();
        $filter = $request->get('filter');
        $filter['type'] = 1;
        
        $benefits = $em->getRepository('AppBundle:Benefit')->getBenefitsByFilter($filter);

        $search['filter'] = array();
        $search['filter']['type'] = $filter['type'];
        if(isset($filter['name'])){
            $search['filter']['name'] = $filter['name'];
        }else{
            $search['filter']['name'] = '';
        }
        if(isset($filter['dni'])){
            $search['filter']['dni'] = $filter['dni'];
        }else{
            $search['filter']['dni'] = '';
        }
        if(isset($filter['type'])){
            $search['filter']['type'] = $filter['type'];
        }else{
            $search['filter']['type'] = '';
        }
        if(isset($filter['passport'])){
            $search['filter']['passport'] = $filter['passport'];
        }else{
            $search['filter']['passport'] = '';
        }
        if(isset($filter['cuil'])){
            $search['filter']['cuil'] = $filter['cuil'];
        }else{
            $search['filter']['cuil'] = '';
        }
        if(isset($filter['area'])){
            $search['filter']['area'] = $filter['area'];
        }else{
            $search['filter']['area'] = '';
        }
        if(isset($filter['ministry'])){
            $search['filter']['ministry'] = $filter['ministry'];
        }else{
            $search['filter']['ministry'] = '';
        }
        if(isset($filter['secretariat'])){
            $search['filter']['secretariat'] = $filter['secretariat'];
        }else{
            $search['filter']['secretariat'] = '';
        }
        if(isset($filter['direction'])){
            $search['filter']['direction'] = $filter['direction'];
        }else{
            $search['filter']['direction'] = '';
        }

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $benefits, 
            $request->query->getInt('page', 1),
            20
        );
        
        $areas = $em->getRepository('AppBundle:Area')->findAll();
        $ministries = $em->getRepository('AppBundle:Ministry')->findAll();
        $secretariats = $em->getRepository('AppBundle:Secretariat')->findAll();
        $directions = $em->getRepository('AppBundle:Direction')->findAll();

        return $this->render('AppBundle:benefit:becas.html.twig', array(
            'pagination' => $pagination,
            'types'    => Benefit::TYPE,
            'filter'   => $filter,
            'search'   => $search,
            'areas'    => $areas,
            'ministries'    => $ministries,
            'secretariats' => $secretariats,
            'directions' => $directions,
        ));
    }

    /**
     * Creates a new benefit entity.
     *
     * @Route("/new", name="benefit_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $benefit = new Benefit();
        $form = $this->createForm('AppBundle\Form\BenefitType', $benefit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $b = true;
            $em = $this->getDoctrine()->getManager();

            if( $benefit->getDni() && $em->getRepository('AppBundle:Benefit')->findByDni($benefit->getDni())){
                $this->addFlash('error','El DNI ingresado ya existe en la base de datos.');
                $b = false;
            }
            if( $benefit->getPassport() && $em->getRepository('AppBundle:Benefit')->findByPassport($benefit->getPassport())){
                $this->addFlash('error','El pasaporte ingresado ya existe en la base de datos.');
                $b = false;
            }

            if($b){
                foreach($benefit->getPdfs() as $pdf){
                    $file = new FilePdf();
                    $file->setFile($pdf);
                    $benefit->addFilePdf($file);
                }
                $benefit->setUser($this->getUser());
                $em->persist($benefit);
                $em->flush();

                foreach($benefit->getFilePdfs() as $file){
                    $file->upload();
                }
                $em->persist($benefit);
                $em->flush();
                $this->addFlash('success','Se agregó el beneficio con éxito');

                return $this->redirectToRoute('benefit_index');
            }else{
                return $this->render('AppBundle:benefit:new.html.twig', array(
                    'benefit' => $benefit,
                    'form' => $form->createView(),
                ));
            }


        }

        return $this->render('AppBundle:benefit:new.html.twig', array(
            'benefit' => $benefit,
            'form' => $form->createView(),
        ));
    }

    /**
     * Deletes a benefit entity.
     *
     * @Route("/{id}/pdf/{id_pdf}/delete", name="benefit_pdf_delete")
     * @Method("GET")
     */
    public function deletePdfAction($id,$id_pdf)
    {
        if(!$this->isGranted(['ROLE_LEVEL3','ROLE_FULL'])){
            $this->addFlash('error','Usted no tiene permisos para realizar esta acción');
            return $this->redirectToRoute('benefit_index');            
        }
        $em = $this->getDoctrine()->getManager();
        $benefit = $em->getRepository('AppBundle:Benefit')->find($id);
        $pdf = $em->getRepository('AppBundle:FilePdf')->find($id_pdf);

        $benefit->removeFilePdf($pdf);
        $pdf->delete();
        $em->remove($pdf);
        $em->flush();
        $this->addFlash('success','Se eliminó el pdf correctamente.');

        return $this->redirectToRoute('benefit_edit',array('id'=>$benefit->getId()));
    }

    /**
     * Finds and displays a benefit entity.
     *
     * @Route("/{id}", name="benefit_show")
     * @Method("GET")
     */
    public function showAction(Benefit $benefit)
    {
        $deleteForm = $this->createDeleteForm($benefit);
        $filesNames = array(1=>'',2=>'',3=>'',4=>'',5=>'');
        foreach($benefit->getFilePdfs() as $file){
            $keys = explode('_', $file->getName());
            $filesNames[$keys[0]] = $file->getWebPath();
        }

        return $this->render('AppBundle:benefit:show.html.twig', array(
            'benefit' => $benefit,
            'delete_form' => $deleteForm->createView(),
            'files' => $filesNames
        ));
    }

    /**
     * Displays a form to edit an existing benefit entity.
     *
     * @Route("/{id}/edit", name="benefit_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Benefit $benefit)
    {
        $deleteForm = $this->createDeleteForm($benefit);
        $editForm = $this->createForm('AppBundle\Form\BenefitType', $benefit);
        $editForm->handleRequest($request);
        $filesNames = array(1=>array(0=>'',1=>''),2=>array(0=>'',1=>''),3=>array(0=>'',1=>''),4=>array(0=>'',1=>''),5=>array(0=>'',1=>''));

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();

            /*
            * Código para actualizar los campos pdf. Por falta de tiempo tuve que hacerlo así. 
            */
            $na = $request->files->get('benefit');
            if($na['pdfs'][0] != null){
                foreach($benefit->getFilePdfs() as $file){
                    $name = '1_'.$benefit->getId().'.pdf';
                    if($file->getName() == $name){
                        $file->delete();
                        $benefit->removeFilePdf($file);
                        $em->remove($file);
                    }
                }
            }
            if($na['pdfs'][1] != null){
                foreach($benefit->getFilePdfs() as $file){
                    $name = '2_'.$benefit->getId().'.pdf';
                    if($file->getName() == $name){
                        $file->delete();
                        $benefit->removeFilePdf($file);
                        $em->remove($file);
                    }
                }
            }
            if($na['pdfs'][2] != null){
                foreach($benefit->getFilePdfs() as $file){
                    $name = '3_'.$benefit->getId().'.pdf';
                    if($file->getName() == $name){
                        $file->delete();
                        $benefit->removeFilePdf($file);
                        $em->remove($file);
                    }
                }
            }
            if($na['pdfs'][3] != null){
                foreach($benefit->getFilePdfs() as $file){
                    $name = '4_'.$benefit->getId().'.pdf';
                    if($file->getName() == $name){
                        $file->delete();
                        $benefit->removeFilePdf($file);
                        $em->remove($file);
                    }
                }
            }
            if($na['pdfs'][4] != null){
                foreach($benefit->getFilePdfs() as $file){
                    $name = '5_'.$benefit->getId().'.pdf';
                    if($file->getName() == $name){
                        $file->delete();
                        $benefit->removeFilePdf($file);
                        $em->remove($file);
                    }
                }
            }


            foreach($benefit->getPdfs() as $pdf){
                $file = new FilePdf();
                $file->setFile($pdf);
                $benefit->addFilePdf($file);
            }
            
            foreach($benefit->getFilePdfs() as $file){
                $file->upload();
            }
            $em->flush();

            $this->addFlash('success','Se modificó el beneficio con éxito');

            return $this->redirectToRoute('benefit_index');
        }else{
            foreach($benefit->getFilePdfs() as $file){
                $keys = explode('_', $file->getName());
                $filesNames[$keys[0]][0] = $file->getWebPath();
                $filesNames[$keys[0]][1] = $file->getId();
            }
        }
        
        return $this->render('AppBundle:benefit:edit.html.twig', array(
            'benefit' => $benefit,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'files' => $filesNames
        ));
    }

    /**
     * Deletes a benefit entity.
     *
     * @Route("/{id}/delete", name="benefit_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, Benefit $benefit)
    {
        if(!$this->isGranted(['ROLE_LEVEL3','ROLE_FULL'])){
            $this->addFlash('error','Usted no tiene permisos para realizar esta acción');
            return $this->redirectToRoute('benefit_index');            
        }
        foreach($benefit->getFilePdfs() as $file){
            $file->delete();
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($benefit);
        $em->flush();
        $this->addFlash('success','Se eliminó el registro correctamente.');

        return $this->redirectToRoute('benefit_index');
    }

    /**
     * Creates a form to delete a benefit entity.
     *
     * @param Benefit $benefit The benefit entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Benefit $benefit)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('benefit_delete', array('id' => $benefit->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
