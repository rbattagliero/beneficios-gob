<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->redirectToRoute('benefit_index');
    }

    /**
     * @Route("/admin/test", name="test")
     */
    public function testAction(Request $request)
    {
        return $this->render('default/index2.html.twig');
    }

    /**
     * @Route("/users/index", name="user_index")
     */
    public function userIndexAction(Request $request)
    {
        return $this->render('AppBundle:users:index.html.twig');
    }

    /**
     * @Route("/users/new", name="user_new")
     * @Method({"GET", "POST"})
     */
    public function userNewAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = new User();
        $form = $this->createForm('AppBundle\Form\UserType', $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($etec);
            $em->flush();

            $this->addFlash('success','Éxito!.' );
            return $this->render('AppBundle:users:index.html.twig');
        }
        return $this->render('AppBundle:users:new.html.twig');
    }
}
