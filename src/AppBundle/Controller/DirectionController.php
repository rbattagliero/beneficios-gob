<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Direction;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Direction controller.
 *
 * @Route("direction")
 */
class DirectionController extends Controller
{
    /**
     * Lists all direction entities.
     *
     * @Route("/", name="direction_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $filter = array();
        $filter = $request->get('filter');
        
        $directions = $em->getRepository('AppBundle:Direction')->getBenefitsByFilter($filter);

        return $this->render('AppBundle:direction:index.html.twig', array(
            'directions' => $directions,
            'filter'   => $filter
        ));
    }

    /**
     * Creates a new direction entity.
     *
     * @Route("/new", name="direction_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $direction = new Direction();
        $form = $this->createForm('AppBundle\Form\DirectionType', $direction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if(!$em->getRepository('AppBundle:Direction')->findByName($direction->getName())){
                $em->persist($direction);
                $em->flush();
                $this->addFlash('success','Se agregó el área correctamente.');
                return $this->redirectToRoute('direction_index');
            }else{
                $this->addFlash('error','El nombre ya existe.');
            }
        }

        return $this->render('AppBundle:direction:new.html.twig', array(
            'direction' => $direction,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a direction entity.
     *
     * @Route("/{id}", name="direction_show")
     * @Method("GET")
     */
    public function showAction(Direction $direction)
    {
        $deleteForm = $this->createDeleteForm($direction);

        return $this->render('AppBundle:direction:show.html.twig', array(
            'direction' => $direction,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing direction entity.
     *
     * @Route("/{id}/edit", name="direction_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Direction $direction)
    {
        $deleteForm = $this->createDeleteForm($direction);
        $editForm = $this->createForm('AppBundle\Form\DirectionType', $direction);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success','Se modificó el área correctamente.');
            return $this->redirectToRoute('direction_index');
        }

        return $this->render('AppBundle:direction:edit.html.twig', array(
            'direction' => $direction,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a direction entity.
     *
     * @Route("/{id}/delete", name="direction_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, Direction $direction)
    {
        try{
            $em = $this->getDoctrine()->getManager();
            $em->remove($direction);
            $em->flush();

            $this->addFlash('success','Se eliminó correctamente el registro.');
            
        }catch(ForeignKeyConstraintViolationException $e){
            $this->addFlash('error','No se puede eliminar el registro porque tiene beneficiarios asociados. Elmine la relación e intente de nuevo.');
        }

        return $this->redirectToRoute('direction_index');
    }

    /**
     * Creates a form to delete a direction entity.
     *
     * @param Direction $direction The direction entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Direction $direction)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('direction_delete', array('id' => $direction->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
