<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Ministry;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Ministry controller.
 *
 * @Route("ministry")
 */
class MinistryController extends Controller
{
    /**
     * Lists all ministry entities.
     *
     * @Route("/", name="ministry_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $filter = array();
        $filter = $request->get('filter');
        
        $ministries = $em->getRepository('AppBundle:Ministry')->getBenefitsByFilter($filter);

        return $this->render('AppBundle:ministry:index.html.twig', array(
            'ministries' => $ministries,
            'filter'   => $filter
        ));
    }

    /**
     * Creates a new ministry entity.
     *
     * @Route("/new", name="ministry_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $ministry = new Ministry();
        $form = $this->createForm('AppBundle\Form\MinistryType', $ministry);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if(!$em->getRepository('AppBundle:Ministry')->findByName($ministry->getName())){
                $em->persist($ministry);
                $em->flush();
                $this->addFlash('success','Se agregó el registro correctamente.');
                return $this->redirectToRoute('ministry_index');
            }else{
                $this->addFlash('error','El nombre ya existe.');
            }
        }

        return $this->render('AppBundle:ministry:new.html.twig', array(
            'ministry' => $ministry,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ministry entity.
     *
     * @Route("/{id}", name="ministry_show")
     * @Method("GET")
     */
    public function showAction(Ministry $ministry)
    {
        $deleteForm = $this->createDeleteForm($ministry);

        return $this->render('AppBundle:ministry:show.html.twig', array(
            'ministry' => $ministry,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing ministry entity.
     *
     * @Route("/{id}/edit", name="ministry_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Ministry $ministry)
    {
        $deleteForm = $this->createDeleteForm($ministry);
        $editForm = $this->createForm('AppBundle\Form\MinistryType', $ministry);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success','Se modificó el registro correctamente.');

            return $this->redirectToRoute('ministry_index');
        }

        return $this->render('AppBundle:ministry:edit.html.twig', array(
            'ministry' => $ministry,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a ministry entity.
     *
     * @Route("/{id}/delete", name="ministry_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, Ministry $ministry)
    {
        try{
            $em = $this->getDoctrine()->getManager();
            $em->remove($ministry);
            $em->flush();

            $this->addFlash('success','Se eliminó correctamente el registro.');
            
        }catch(ForeignKeyConstraintViolationException $e){
            $this->addFlash('error','No se puede eliminar el registro porque tiene beneficiarios asociados. Elmine la relación e intente de nuevo.');
        }

        return $this->redirectToRoute('ministry_index');
    }

    /**
     * Creates a form to delete a ministry entity.
     *
     * @param Ministry $ministry The ministry entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Ministry $ministry)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('ministry_delete', array('id' => $ministry->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
