<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Secretariat;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Secretariat controller.
 *
 * @Route("secretariat")
 */
class SecretariatController extends Controller
{
    /**
     * Lists all secretariat entities.
     *
     * @Route("/", name="secretariat_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $filter = array();
        $filter = $request->get('filter');
        
        $secretariats = $em->getRepository('AppBundle:Secretariat')->getBenefitsByFilter($filter);

        return $this->render('AppBundle:secretariat:index.html.twig', array(
            'secretariats' => $secretariats,
            'filter'   => $filter
        ));
    }

    /**
     * Creates a new secretariat entity.
     *
     * @Route("/new", name="secretariat_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $secretariat = new Secretariat();
        $form = $this->createForm('AppBundle\Form\SecretariatType', $secretariat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if(!$em->getRepository('AppBundle:Secretariat')->findByName($secretariat->getName())){
                $em->persist($secretariat);
                $em->flush();
                $this->addFlash('success','Se agregó el registro correctamente.');
                return $this->redirectToRoute('secretariat_index');
            }else{
                $this->addFlash('error','El nombre ya existe.');
            }
        }

        return $this->render('AppBundle:secretariat:new.html.twig', array(
            'secretariat' => $secretariat,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a secretariat entity.
     *
     * @Route("/{id}", name="secretariat_show")
     * @Method("GET")
     */
    public function showAction(Secretariat $secretariat)
    {
        $deleteForm = $this->createDeleteForm($secretariat);

        return $this->render('AppBundle:secretariat:show.html.twig', array(
            'secretariat' => $secretariat,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing secretariat entity.
     *
     * @Route("/{id}/edit", name="secretariat_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Secretariat $secretariat)
    {
        $deleteForm = $this->createDeleteForm($secretariat);
        $editForm = $this->createForm('AppBundle\Form\SecretariatType', $secretariat);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success','Se modificó el registro correctamente.');
            return $this->redirectToRoute('secretariat_index');
        }

        return $this->render('AppBundle:secretariat:edit.html.twig', array(
            'secretariat' => $secretariat,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a secretariat entity.
     *
     * @Route("/{id}/delete", name="secretariat_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, Secretariat $secretariat)
    {
        try{
            $em = $this->getDoctrine()->getManager();
            $em->remove($secretariat);
            $em->flush();

            $this->addFlash('success','Se eliminó correctamente el registro.');
            
        }catch(ForeignKeyConstraintViolationException $e){
            $this->addFlash('error','No se puede eliminar el registro porque tiene beneficiarios asociados. Elmine la relación e intente de nuevo.');
        }

        return $this->redirectToRoute('secretariat_index');
    }

    /**
     * Creates a form to delete a secretariat entity.
     *
     * @param Secretariat $secretariat The secretariat entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Secretariat $secretariat)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('secretariat_delete', array('id' => $secretariat->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
