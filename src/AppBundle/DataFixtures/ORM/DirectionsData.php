<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Direction;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DirectionsData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager) {

        $csv = fopen($this->container->get('kernel')->getRootDir().'/../csv/directions.csv', 'r');
        $i = 0;

        $metadata = $manager->getClassMetaData(get_class(new Direction()));
        $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);

        while (!feof($csv)) {
            $line = fgetcsv($csv,null,';');

            $direction[$i] = new Direction();
            $direction[$i]->setId($line[0]);
            $direction[$i]->setName($line[1]);
            $manager->persist($direction[$i]);
            $i = $i + 1;
        }

        fclose($csv);
        $manager->flush();

    }

    public function getOrder() {
        return 1;
    }
}