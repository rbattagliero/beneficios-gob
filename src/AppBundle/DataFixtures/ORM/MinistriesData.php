<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Ministry;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MinistriesData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager) {

        $csv = fopen($this->container->get('kernel')->getRootDir().'/../csv/ministries.csv', 'r');
        $i = 0;

        $metadata = $manager->getClassMetaData(get_class(new Ministry()));
        $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);

        while (!feof($csv)) {
            $line = fgetcsv($csv,null,';');

            $ministry[$i] = new Ministry();
            $ministry[$i]->setId($line[0]);
            $ministry[$i]->setName($line[1]);
            $manager->persist($ministry[$i]);
            $i = $i + 1;
        }

        fclose($csv);
        $manager->flush();

    }

    public function getOrder() {
        return 1;
    }
}