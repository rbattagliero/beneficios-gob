<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Secretariat;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SecretariatsData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager) {

        $csv = fopen($this->container->get('kernel')->getRootDir().'/../csv/secretariats.csv', 'r');
        $i = 0;

        $metadata = $manager->getClassMetaData(get_class(new Secretariat()));
        $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);

        while (!feof($csv)) {
            $line = fgetcsv($csv,null,';');

            $secretariat[$i] = new Secretariat();
            $secretariat[$i]->setId($line[0]);
            $secretariat[$i]->setName($line[1]);
            $manager->persist($secretariat[$i]);
            $i = $i + 1;
        }

        fclose($csv);
        $manager->flush();

    }

    public function getOrder() {
        return 1;
    }
}