<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * @ORM\Table(name="benefit")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BenefitRepository")
 */
class Benefit
{
    const TYPE = array(1=>'Beca', 2=>'Contrato');
    const TYPE_OPTIONS = array('Beca'=>1, 'Contrato'=>2);
	/**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
	private $id;

	/**
     * @ORM\Column(name="type", type="integer", nullable=true)
     * @Assert\NotNull(groups={"benefit"})
     * @Assert\Choice(choices = {1, 2}, groups={"benefit"})
     */
	private $type;

	/**
     * @var Area
     *
     * @ORM\ManyToOne(targetEntity="Area")
     */
	private $area;

	/**
     * @var Secretariat
     *
     * @ORM\ManyToOne(targetEntity="Secretariat")
     */
	private $secretariat;

    /**
     * @var Direction
     *
     * @ORM\ManyToOne(targetEntity="Direction")
     */
    private $direction;

	/**
     * @var Ministry
     *
     * @ORM\ManyToOne(targetEntity="Ministry")
     */
	private $ministry;

	/**
    * @ORM\Column(name="area_manager", type="string", length=200, nullable=true)
    * @Assert\Length(max=200, groups={"benefit_user"})
    */
	private $areaManager;

    /**
    * @ORM\Column(name="institution_other", type="string", length=200, nullable=true)
    * @Assert\Length(max=200, groups={"benefit_user"})
    */
    private $institutionOther;


    /**
    * @ORM\OneToMany(targetEntity="FilePdf", mappedBy="benefit",cascade={"all"})
    */
	private $filePdfs;

    private $pdfs;

    /**
    * @ORM\ManyToOne(targetEntity="User")
    */
    private $user;

    public function __construct()
    {
        $this->setNationality('Argentina');
        $this->setZipCode('5300');
        $this->filePdfs = new ArrayCollection();
        $this->pdfs = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     *
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function getTypeName()
    {
        return ($this->getType()) ? self::TYPE[$this->getType()] : '';
    }

    public function getBloodTypeName()
    {
        return ($this->getBloodType()) ? self::BLOOD_TYPE[$this->getBloodType()] : '';
    }

    public function getCivilStatusName()
    {
        return self::CIVIL_STATUS[$this->getCivilStatus()];
    }

    /**
     * @return mixed
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param mixed $area
     *
     * @return self
     */
    public function setArea(Area $area = null)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSecretariat()
    {
        return $this->secretariat;
    }

    /**
     * @param mixed $secretariat
     *
     * @return self
     */
    public function setSecretariat(Secretariat $secretariat = null)
    {
        $this->secretariat = $secretariat;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMinistry()
    {
        return $this->ministry;
    }

    /**
     * @param mixed $ministry
     *
     * @return self
     */
    public function setMinistry(Ministry $ministry = null)
    {
        $this->ministry = $ministry;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAreaManager()
    {
        return $this->areaManager;
    }

    /**
     * @param mixed $areaManager
     *
     * @return self
     */
    public function setAreaManager($areaManager)
    {
        $this->areaManager = $areaManager;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFilePdfs()
    {
        return $this->filePdfs;
    }

    /**
     * @param mixed $files
     *
     * @return self
     */
    public function setFilePdfs($files)
    {
        $this->filePdfs = $filePdfs;

        return $this;
    }

    public function addFilePdf(FilePdf $file)
    {
        $this->filePdfs->add($file);
        $file->setBenefit($this);
    }

    public function removeFilePdf(FilePdf $file)
    {
        $this->filePdfs->removeElement($file);
        $file->setBenefit(null);
    }

    const BLOOD_TYPE = array(0=>'',1=>'0-',2 => '0+',3=>'A-',4=>'A+',5=>'B-',6=>'B+',7=>'AB-',8=>'AB+');
    const BLOOD_TYPE_OPTIONS = array(''=>0,'0-'=>1,'0+'=>2,'A-'=>3,'A+'=>4,'B-'=>5,'B+'=>6,'AB-'=>7,'AB+'=>8);
    const CIVIL_STATUS = array(0=>'Soltero',1=>'Casado',2=>'Viudo',3=>'Divorsiado');
    const CIVIL_STATUS_OPTIONS = array('Soltero'=>0,'Casado'=>1,'Viudo'=>2,'Divorsiado'=>3);

    /************************************************************************
    *
    * Basic data
    *
    *************************************************************************/

    /**
    * @ORM\Column(name="name", type="string", length=200, nullable=true)
    * @Assert\NotBlank(groups={"benefit_user"})
    * @Assert\Length(max=200, groups={"benefit_user"})
    */
    private $name;

    /**
    * @ORM\Column(name="lastname", type="string", length=200, nullable=true)
    * @Assert\NotBlank(groups={"benefit_user"})
    * @Assert\Length(max=200, groups={"benefit_user"})
    */
    private $lastname;

    /**
    * @ORM\Column(name="dni", type="string", length=10, nullable=true)
    * @Assert\Length(max=8, groups={"benefit_user"})
    */
    private $dni;

    /**
    * @ORM\Column(name="passport", type="string", length=10, nullable=true)
    * @Assert\Length(max=9, groups={"benefit_user"})
    */
    private $passport;

    /**
    * @ORM\Column(name="cuil", type="string", length=15, nullable=true)
    * @Assert\NotBlank(groups={"benefit_user"})
    * @Assert\Length(max=13, groups={"benefit_user"})
    */
    private $cuil;

    /**
    * @ORM\Column(name="birthday", type="date", nullable=true)
    * @Assert\Date(groups={"benefit_user"})
    */
    private $birthday;

    /**
    * @ORM\Column(name="place", type="string", length=255, nullable=true)
    * @Assert\Length(max=255, groups={"benefit_user"})
    */
    private $place;

    /**
    * @ORM\Column(name="nationality", type="string", length=255, nullable=true)
    * @Assert\Length(max=255, groups={"benefit_user"})
    */
    private $nationality;

    /**
    * @ORM\Column(name="blood_type", type="integer", nullable=true)
    * @Assert\NotBlank(groups={"benefit_user"})
    * @Assert\Choice(choices = {0, 1, 2, 3, 4, 5}, groups={"benefit_user"})
    */
    private $bloodType;

    /**
    * @ORM\Column(name="civil_status", type="integer", nullable=true)
    * @Assert\NotBlank(groups={"benefit_user"})
    * @Assert\Choice(choices = {0, 1, 2, 3, 4, 5}, groups={"benefit_user"})
    */
    private $civilStatus;

    /**
    * @ORM\Column(name="phone", type="string", length=255, nullable=true)
    * @Assert\NotBlank(groups={"benefit_user"})
    * @Assert\Length(max=255, groups={"benefit_user"})
    */
    private $phone;

    /**
    * @ORM\Column(name="email", type="string", length=255, nullable=true)
    * @Assert\Email(groups={"benefit_user"})
    * @Assert\Length(max=255, groups={"benefit_user"})
    */
    private $email;

    /************************************************************************
    *
    * Address data
    *
    *************************************************************************/

    /**
    * @ORM\Column(name="street", type="string", length=255, nullable=true)
    * @Assert\Email(groups={"benefit_user"})
    * @Assert\Length(max=255, groups={"benefit_user"})
    */
    private $street;

    /**
    * @ORM\Column(name="neighborhood", type="string", length=255, nullable=true)
    * @Assert\Email(groups={"benefit_user"})
    * @Assert\Length(max=255, groups={"benefit_user"})
    */
    private $neighborhood;

    /**
    * @ORM\Column(name="city", type="string", length=255, nullable=true)
    * @Assert\Email(groups={"benefit_user"})
    * @Assert\Length(max=255, groups={"benefit_user"})
    */
    private $city;

    /**
    * @ORM\Column(name="depto", type="string", length=255, nullable=true)
    * @Assert\Email(groups={"benefit_user"})
    * @Assert\Length(max=255, groups={"benefit_user"})
    */
    private $depto;

    /**
    * @ORM\Column(name="zipCode", type="integer", length=4, nullable=true)
    * @Assert\Email(groups={"benefit_user"})
    * @Assert\Length(max=4, groups={"benefit_user"})
    */
    private $zipCode;

    /************************************************************************
    *
    * Study data
    *
    *************************************************************************/

    /**
    * @ORM\Column(name="is_primary_school", type="boolean", nullable=true)
    */
    private $isPrimarySchool;

    /**
    * @ORM\Column(name="is_basic_cycle", type="boolean", nullable=true)
    */
    private $isBasicCycle;

    /**
    * @ORM\Column(name="is_high_school", type="boolean", nullable=true)
    */
    private $isHighSchool;

    /**
    * @ORM\Column(name="is_tertiary_school", type="boolean", nullable=true)
    */
    private $isTertiarySchool;

    /**
    * @ORM\Column(name="is_universitary_degree", type="boolean", nullable=true)
    */
    private $isUniversitaryDegree;

    /**
    * @ORM\Column(name="is_postgrade", type="boolean", nullable=true)
    */
    private $isPostgrade;

    /**
    * @ORM\Column(name="degree", type="string", length=255, nullable=true)
    * @Assert\Email(groups={"benefit_user"})
    * @Assert\Length(max=255, groups={"benefit_user"})
    */
    private $degree;

    /**
    * @ORM\Column(name="actual_study", type="string", length=255, nullable=true)
    * @Assert\Email(groups={"benefit_user"})
    * @Assert\Length(max=255, groups={"benefit_user"})
    */
    private $actualStudy;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getFullName()
    {
        return $this->getLastname() . ', ' . $this->getName();
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     *
     * @return self
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDni()
    {
        return $this->dni;
    }

    /**
     * @param mixed $dni
     *
     * @return self
     */
    public function setDni($dni)
    {
        $this->dni = $dni;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCuil()
    {
        return $this->cuil;
    }

    /**
     * @param mixed $cuil
     *
     * @return self
     */
    public function setCuil($cuil)
    {
        $this->cuil = $cuil;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param mixed $birthday
     *
     * @return self
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * @param mixed $place
     *
     * @return self
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * @param mixed $nationality
     *
     * @return self
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBloodType()
    {
        return $this->bloodType;
    }

    /**
     * @param mixed $bloodType
     *
     * @return self
     */
    public function setBloodType($bloodType)
    {
        $this->bloodType = $bloodType;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCivilStatus()
    {
        return $this->civilStatus;
    }

    /**
     * @param mixed $civilStatus
     *
     * @return self
     */
    public function setCivilStatus($civilStatus)
    {
        $this->civilStatus = $civilStatus;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     *
     * @return self
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param mixed $street
     *
     * @return self
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNeighborhood()
    {
        return $this->neighborhood;
    }

    /**
     * @param mixed $neighborhood
     *
     * @return self
     */
    public function setNeighborhood($neighborhood)
    {
        $this->neighborhood = $neighborhood;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     *
     * @return self
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDepto()
    {
        return $this->depto;
    }

    /**
     * @param mixed $depto
     *
     * @return self
     */
    public function setDepto($depto)
    {
        $this->depto = $depto;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * @param mixed $zipCode
     *
     * @return self
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsPrimarySchool()
    {
        return $this->isPrimarySchool;
    }

    /**
     * @param mixed $isPrimarySchool
     *
     * @return self
     */
    public function setIsPrimarySchool($isPrimarySchool)
    {
        $this->isPrimarySchool = $isPrimarySchool;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsBasicCycle()
    {
        return $this->isBasicCycle;
    }

    /**
     * @param mixed $isBasicCycle
     *
     * @return self
     */
    public function setIsBasicCycle($isBasicCycle)
    {
        $this->isBasicCycle = $isBasicCycle;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsHighSchool()
    {
        return $this->isHighSchool;
    }

    /**
     * @param mixed $isHighSchool
     *
     * @return self
     */
    public function setIsHighSchool($isHighSchool)
    {
        $this->isHighSchool = $isHighSchool;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsTertiarySchool()
    {
        return $this->isTertiarySchool;
    }

    /**
     * @param mixed $isTertiarySchool
     *
     * @return self
     */
    public function setIsTertiarySchool($isTertiarySchool)
    {
        $this->isTertiarySchool = $isTertiarySchool;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsPostgrade()
    {
        return $this->isPostgrade;
    }

    /**
     * @param mixed $isPostgrade
     *
     * @return self
     */
    public function setIsPostgrade($isPostgrade)
    {
        $this->isPostgrade = $isPostgrade;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDegree()
    {
        return $this->degree;
    }

    /**
     * @param mixed $degree
     *
     * @return self
     */
    public function setDegree($degree)
    {
        $this->degree = $degree;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getActualStudy()
    {
        return $this->actualStudy;
    }

    /**
     * @param mixed $actualStudy
     *
     * @return self
     */
    public function setActualStudy($actualStudy)
    {
        $this->actualStudy = $actualStudy;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsUniversitaryDegree()
    {
        return $this->isUniversitaryDegree;
    }

    /**
     * @param mixed $isUniversitaryDegree
     *
     * @return self
     */
    public function setIsUniversitaryDegree($isUniversitaryDegree)
    {
        $this->isUniversitaryDegree = $isUniversitaryDegree;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassport()
    {
        return $this->passport;
    }

    /**
     * @param mixed $passport
     *
     * @return self
     */
    public function setPassport($passport)
    {
        $this->passport = $passport;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInstitutionOther()
    {
        return $this->institutionOther;
    }

    /**
     * @param mixed $institutionOther
     *
     * @return self
     */
    public function setInstitutionOther($institutionOther)
    {
        $this->institutionOther = $institutionOther;

        return $this;
    }

    /**
     * @return Direction
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * @param Direction $direction
     *
     * @return self
     */
    public function setDirection(Direction $direction = null)
    {
        $this->direction = $direction;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPdfs()
    {
        return $this->pdfs;
    }

    /**
     * @param mixed $pdfs
     *
     * @return self
     */
    public function setPdfs($pdfs)
    {
        $this->pdfs = $pdfs;

        return $this;
    }

    public function addPdf(UploadedFile $file = null)
    {
        if(!$this->pdfs)
        {
            $this->pdfs = new ArrayCollection();
        }
        if($file){
            $this->pdfs->add($file);
        }
    }

    public function removePdf(UploadedFile $file)
    {
        $this->pdfs->removeElement($file);
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     *
     * @return self
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }
}
?>