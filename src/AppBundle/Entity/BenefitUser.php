<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/*
 *
 * @ORM\Table(name="benefit_user")
 * @ORM\Entity
 */
class BenefitUser
{

	const BLOOD_TYPE = array(0=>'A+',1=>'B-');
	const CIVIL_STATUS = array(0=>'Casado',1=>'Soltero');

	/************************************************************************
	*
	* Basic data
	*
	*************************************************************************/
	/**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
	private $id;

	/**
	* @ORM\Column(name="name", type="string", length=200, nullable=true)
	* @Assert\NotBlank(groups={"benefit_user"})
	* @Assert\Length(max=200, groups={"benefit_user"})
	*/
	private $name;

	/**
	* @ORM\Column(name="lastname", type="string", length=200, nullable=true)
	* @Assert\NotBlank(groups={"benefit_user"})
	* @Assert\Length(max=200, groups={"benefit_user"})
	*/
	private $lastname;

	/**
	* @ORM\Column(name="dni", type="string", length=10, nullable=true)
	* @Assert\NotBlank(groups={"benefit_user"})
	* @Assert\Length(max=8, groups={"benefit_user"})
	*/
	private $dni;

	/**
	* @ORM\Column(name="cuil", type="string", length=15, nullable=true)
	* @Assert\NotBlank(groups={"benefit_user"})
	* @Assert\Length(max=13, groups={"benefit_user"})
	*/
	private $cuil;

	/**
	* @ORM\Column(name="birthday", type="date", nullable=true)
	* @Assert\NotBlank(groups={"benefit_user"})
	* @Assert\Date(groups={"benefit_user"})
	*/
	private $birthday;

	/**
	* @ORM\Column(name="place", type="string", length=255, nullable=true)
	* @Assert\NotBlank(groups={"benefit_user"})
	* @Assert\Length(max=255, groups={"benefit_user"})
	*/
	private $place;

	/**
	* @ORM\Column(name="nationality", type="string", length=255, nullable=true)
	* @Assert\NotBlank(groups={"benefit_user"})
	* @Assert\Length(max=255, groups={"benefit_user"})
	*/
	private $nationality;

	/**
	* @ORM\Column(name="blood_type", type="integer", nullable=true)
	* @Assert\NotBlank(groups={"benefit_user"})
	* @Assert\Choice(choices = {1, 2, 3, 4, 5}, groups={"benefit_user"})
	*/
	private $bloodType;

	/**
	* @ORM\Column(name="civil_status", type="integer", nullable=true)
	* @Assert\NotBlank(groups={"benefit_user"})
	* @Assert\Choice(choices = {1, 2, 3, 4, 5}, groups={"benefit_user"})
	*/
	private $civilStatus;

	/**
	* @ORM\Column(name="phone", type="string", length=255, nullable=true)
	* @Assert\NotBlank(groups={"benefit_user"})
	* @Assert\Length(max=255, groups={"benefit_user"})
	*/
	private $phone;

	/**
	* @ORM\Column(name="email", type="string", length=255, nullable=true)
	* @Assert\NotBlank(groups={"benefit_user"})
	* @Assert\Email(groups={"benefit_user"})
	* @Assert\Length(max=255, groups={"benefit_user"})
	*/
	private $email;

	/************************************************************************
	*
	* Address data
	*
	*************************************************************************/

	/**
	* @ORM\Column(name="street", type="string", length=255, nullable=true)
	* @Assert\NotBlank(groups={"benefit_user"})
	* @Assert\Email(groups={"benefit_user"})
	* @Assert\Length(max=255, groups={"benefit_user"})
	*/
	private $street;

	/**
	* @ORM\Column(name="neighborhood", type="string", length=255, nullable=true)
	* @Assert\NotBlank(groups={"benefit_user"})
	* @Assert\Email(groups={"benefit_user"})
	* @Assert\Length(max=255, groups={"benefit_user"})
	*/
	private $neighborhood;

	/**
	* @ORM\Column(name="city", type="string", length=255, nullable=true)
	* @Assert\NotBlank(groups={"benefit_user"})
	* @Assert\Email(groups={"benefit_user"})
	* @Assert\Length(max=255, groups={"benefit_user"})
	*/
	private $city;

	/**
	* @ORM\Column(name="depto", type="string", length=255, nullable=true)
	* @Assert\NotBlank(groups={"benefit_user"})
	* @Assert\Email(groups={"benefit_user"})
	* @Assert\Length(max=255, groups={"benefit_user"})
	*/
	private $depto;

	/**
	* @ORM\Column(name="zipCode", type="integer", length=4, nullable=true)
	* @Assert\NotBlank(groups={"benefit_user"})
	* @Assert\Email(groups={"benefit_user"})
	* @Assert\Length(max=4, groups={"benefit_user"})
	*/
	private $zipCode;

	/************************************************************************
	*
	* Study data
	*
	*************************************************************************/

	/**
	* @ORM\Column(name="is_primary_school", type="integer", nullable=true)
	* @Assert\NotBlank(groups={"benefit_user"})
	* @Assert\Choice(choices = {0, 1}, groups={"benefit_user"})
	*/
	private $isPrimarySchool;

	/**
	* @ORM\Column(name="is_basic_cycle", type="integer", nullable=true)
	* @Assert\NotBlank(groups={"benefit_user"})
	* @Assert\Choice(choices = {0, 1}, groups={"benefit_user"})
	*/
	private $isBasicCycle;

	/**
	* @ORM\Column(name="is_high_school", type="integer", nullable=true)
	* @Assert\NotBlank(groups={"benefit_user"})
	* @Assert\Choice(choices = {0, 1}, groups={"benefit_user"})
	*/
	private $isHighSchool;

	/**
	* @ORM\Column(name="is_tertiary_school", type="integer", nullable=true)
	* @Assert\NotBlank(groups={"benefit_user"})
	* @Assert\Choice(choices = {0, 1}, groups={"benefit_user"})
	*/
	private $isTertiarySchool;

	/**
	* @ORM\Column(name="is_postgrade", type="integer", nullable=true)
	* @Assert\NotBlank(groups={"benefit_user"})
	* @Assert\Choice(choices = {0, 1}, groups={"benefit_user"})
	*/
	private $isPostgrade;

	/**
	* @ORM\Column(name="degree", type="string", length=255, nullable=true)
	* @Assert\NotBlank(groups={"benefit_user"})
	* @Assert\Email(groups={"benefit_user"})
	* @Assert\Length(max=255, groups={"benefit_user"})
	*/
	private $degree;

	/**
	* @ORM\Column(name="actual_study", type="string", length=255, nullable=true)
	* @Assert\NotBlank(groups={"benefit_user"})
	* @Assert\Email(groups={"benefit_user"})
	* @Assert\Length(max=255, groups={"benefit_user"})
	*/
	private $actualStudy;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     *
     * @return self
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDni()
    {
        return $this->dni;
    }

    /**
     * @param mixed $dni
     *
     * @return self
     */
    public function setDni($dni)
    {
        $this->dni = $dni;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCuil()
    {
        return $this->cuil;
    }

    /**
     * @param mixed $cuil
     *
     * @return self
     */
    public function setCuil($cuil)
    {
        $this->cuil = $cuil;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param mixed $birthday
     *
     * @return self
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * @param mixed $place
     *
     * @return self
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * @param mixed $nationality
     *
     * @return self
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBloodType()
    {
        return $this->bloodType;
    }

    /**
     * @param mixed $bloodType
     *
     * @return self
     */
    public function setBloodType($bloodType)
    {
        $this->bloodType = $bloodType;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCivilStatus()
    {
        return $this->civilStatus;
    }

    /**
     * @param mixed $civilStatus
     *
     * @return self
     */
    public function setCivilStatus($civilStatus)
    {
        $this->civilStatus = $civilStatus;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     *
     * @return self
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param mixed $street
     *
     * @return self
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNeighborhood()
    {
        return $this->neighborhood;
    }

    /**
     * @param mixed $neighborhood
     *
     * @return self
     */
    public function setNeighborhood($neighborhood)
    {
        $this->neighborhood = $neighborhood;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     *
     * @return self
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDepto()
    {
        return $this->depto;
    }

    /**
     * @param mixed $depto
     *
     * @return self
     */
    public function setDepto($depto)
    {
        $this->depto = $depto;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * @param mixed $zipCode
     *
     * @return self
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsPrimarySchool()
    {
        return $this->isPrimarySchool;
    }

    /**
     * @param mixed $isPrimarySchool
     *
     * @return self
     */
    public function setIsPrimarySchool($isPrimarySchool)
    {
        $this->isPrimarySchool = $isPrimarySchool;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsBasicCycle()
    {
        return $this->isBasicCycle;
    }

    /**
     * @param mixed $isBasicCycle
     *
     * @return self
     */
    public function setIsBasicCycle($isBasicCycle)
    {
        $this->isBasicCycle = $isBasicCycle;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsHighSchool()
    {
        return $this->isHighSchool;
    }

    /**
     * @param mixed $isHighSchool
     *
     * @return self
     */
    public function setIsHighSchool($isHighSchool)
    {
        $this->isHighSchool = $isHighSchool;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsTertiarySchool()
    {
        return $this->isTertiarySchool;
    }

    /**
     * @param mixed $isTertiarySchool
     *
     * @return self
     */
    public function setIsTertiarySchool($isTertiarySchool)
    {
        $this->isTertiarySchool = $isTertiarySchool;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsPostgrade()
    {
        return $this->isPostgrade;
    }

    /**
     * @param mixed $isPostgrade
     *
     * @return self
     */
    public function setIsPostgrade($isPostgrade)
    {
        $this->isPostgrade = $isPostgrade;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDegree()
    {
        return $this->degree;
    }

    /**
     * @param mixed $degree
     *
     * @return self
     */
    public function setDegree($degree)
    {
        $this->degree = $degree;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getActualStudy()
    {
        return $this->actualStudy;
    }

    /**
     * @param mixed $actualStudy
     *
     * @return self
     */
    public function setActualStudy($actualStudy)
    {
        $this->actualStudy = $actualStudy;

        return $this;
    }
}
?>