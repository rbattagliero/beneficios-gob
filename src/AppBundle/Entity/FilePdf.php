<?php

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 *
 * @ORM\Table(name="file_pdf")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class FilePdf
{
	/**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
	private $id;

	/**
    * @ORM\Column(name="name", type="string", length=255, nullable=true)
    */
	private $name;

	/**
	 * @var Benefit
	 * 
     * @ORM\ManyToOne(targetEntity="Benefit", inversedBy="filePdfs")
     */
	private $benefit;

	/**
     * @Assert\Expression(expression="this.getPath() != null || this.getFile() != ''",
     *                    message="error.seleccionarArchivo")
     */
    private $file;

    public function onPersist()
    {
    	$this->upload();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function upload()
    {
    	$file = $this->getFile();
        if ( $file ) {
            $uniqueSafeFilename = $this->safeName($file->getClientOriginalName());
            $uniqueSafeFilename .= '_' . $this->getBenefit()->getId().'.pdf';
            $this->setName($uniqueSafeFilename);

            $file->move(
                $this->getUploadRootDir(),
                $uniqueSafeFilename
            );

        }
    }

    public function getWebPath()
    {
    	return null === $this->getName()
            ? null
            : '/'.$this->getUploadDir().'/'.$this->getName();
    }

    public function delete()
    {
    	if(file_exists($this->getWebPath()))
    	{
    		unlink($this->getWebPath());
    	}
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     *
     * @return self
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../web/' .$this->getUploadDir();
    }

    private function safeName($filename) {
        return str_replace('.pdf', '', $filename);
    }

    private function getUploadDir()
    {
    	return 'uploads/files';
    }


    /**
     * @return Benefit
     */
    public function getBenefit()
    {
        return $this->benefit;
    }

    /**
     * @param Benefit $benefit
     *
     * @return self
     */
    public function setBenefit(Benefit $benefit = null)
    {
        $this->benefit = $benefit;

        return $this;
    }
}
?>