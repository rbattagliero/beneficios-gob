<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="user")
 */
class User extends BaseUser
{

    const ROLES_OPTIONS = array('Nivel 1: Solo puede consultar los registros, listados, etc.' => 'ROLE_LEVEL1','Nivel 2: solo puede cargar y consultar la carga.' => 'ROLE_LEVEL2','Nivel 3: igual nivel 1 más modificar y eliminar registros, cargar áreas.' => 'ROLE_LEVEL3','Super admin: acceso full al sistema.' => 'ROLE_FULL');

    const ROLES = array('ROLE_LEVEL1' => 1,'ROLE_LEVEL2' => 2,'ROLE_LEVEL3' => 3,'ROLE_FULL' => 4);
	/**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
	protected $id;

	/**
	* @ORM\Column(name="first_name", type="string", length=200, nullable=true)
	* @Assert\NotBlank(groups={"user"})
	* @Assert\Length(max=200, groups={"user"})
	*/
	private $firstName;

	/**
	* @ORM\Column(name="lastname", type="string", length=200, nullable=true)
	* @Assert\NotBlank(groups={"user"})
	* @Assert\Length(max=200, groups={"user"})
	*/
	private $lastname;

	/**
	* @ORM\Column(name="birthday", type="string", length=200, nullable=true)
	* @Assert\NotBlank(groups={"user"})
	* @Assert\Length(max=200, groups={"user"})
	*/
	private $birthday;

    public function toString()
    {
        return $this->getLastname() + ', ' + $this->getFirstName();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     *
     * @return self
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     *
     * @return self
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param mixed $birthday
     *
     * @return self
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }
}
?>