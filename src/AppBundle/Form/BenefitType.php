<?php

namespace AppBundle\Form;

use AppBundle\Entity\Benefit;
use AppBundle\Form\FilePdfType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BenefitType extends AbstractType
{
	
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('type',ChoiceType::class,array(
				'choices' => Benefit::TYPE_OPTIONS,
				'label' => 'Tipo de beneficio'
			))
			->add('area',EntityType::class,array(
				'class' => 'AppBundle:Area',
				'choice_label' => 'name',
				'choice_value' => 'id',
				'label' => 'Área / Coordinación / Unidad',
				'placeholder' => 'Seleccionar'
			))
			->add('secretariat',EntityType::class,array(
				'class' => 'AppBundle:Secretariat',
				'choice_label' => 'name',
				'choice_value' => 'id',
				'label' => 'Subsecretaría',
				'placeholder' => 'Seleccionar'
			))
			->add('direction',EntityType::class,array(
				'class' => 'AppBundle:Direction',
				'choice_label' => 'name',
				'choice_value' => 'id',
				'label' => 'Dirección',
				'placeholder' => 'Seleccionar'
			))
			->add('ministry',EntityType::class,array(
				'class' => 'AppBundle:Ministry',
				'choice_label' => 'name',
				'choice_value' => 'id',
				'label' => 'Ministerio / Secretaria',
				'placeholder' => 'Seleccionar'
			))
			->add('areaManager',TextType::class,array(
				'label' => 'Encargado del área'
			))
			->add('name',TextType::class,array(
				'label' => 'Nombres'
			))
			->add('lastname',TextType::class,array(
				'label' => 'Apellidos'
			))
			->add('dni',TextType::class,array(
				'label' => 'DNI'
			))
			->add('passport',TextType::class,array(
				'label' => 'Pasaporte',
				'required' => false
			))
			->add('cuil',TextType::class,array(
				'label' => 'Cuil',
				'attr' => array('class'=>'cuil')
			))
			->add('birthday',DateType::class,array(
				'widget' => 'single_text',
				'format' => 'dd-MM-yyyy',
				'label' => 'Nacimiento'
			))
			->add('place',TextType::class,array(
				'label' => 'Lugar de nacimiento'
			))
			->add('city',TextType::class,array(
				'label' => 'Localidad'
			))
			->add('nationality',TextType::class,array(
				'label' => 'Nacionalidad'
			))
			->add('bloodType',ChoiceType::class,array(
				'choices' => Benefit::BLOOD_TYPE_OPTIONS,
				'label' => 'Grupo sanguíneo'
			))
			->add('civilStatus',ChoiceType::class,array(
				'choices' => Benefit::CIVIL_STATUS_OPTIONS,
				'label' => 'Estado civil'
			))
			->add('phone',TextType::class,array(
				'label' => 'Teléfono'
			))
			->add('email',EmailType::class,array(
				'label' => 'Email',
				'required' => false
			))
			->add('street',TextType::class,array(
				'label' => 'Calle'
			))
			->add('neighborhood',TextType::class,array(
				'label' => 'Barrio'
			))
			->add('depto',TextType::class,array(
				'label' => 'Departamento'
			))
			->add('zipCode',TextType::class,array(
				'label' => 'Código postal'
			))
			->add('isPrimarySchool',CheckboxType::class,array(
				'label' => '¿Primaria completo?',
				'required' => false
			))
			->add('isBasicCycle',CheckboxType::class,array(
				'label' => '¿Ciclo básico?',
				'required' => false	
			))
			->add('isHighSchool',CheckboxType::class,array(
				'label' => '¿Secundaria completo?',
				'required' => false
			))
			->add('isTertiarySchool',CheckboxType::class,array(
				'label' => '¿Terciario completo?',
				'required' => false
			))
			->add('isUniversitaryDegree',CheckboxType::class,array(
				'label' => '¿Título universitario?',
				'required' => false
			))
			->add('isPostgrade',CheckboxType::class,array(
				'label' => '¿Posgrado?',
				'required' => false
			))
			->add('degree',TextType::class,array(
				'label' => 'Título obtenido',
				'required' => false
			))
			->add('actualStudy',TextType::class,array(
				'label' => 'Estudios en curso',
				'required' => false
			))
			->add('institutionOther',TextType::class,array(
				'label' => 'Institución / otros',
				'required' => false
			))
			->add('pdfs', FileType::class,array(
				'multiple' => true
			))
			->add('user',EntityType::class,array(
				'class' => 'AppBundle:User',
				//'choice_label' => 'name',
				'choice_value' => 'id',
				'label' => 'Usuario',
				'placeholder' => 'Seleccionar'
			))
		;
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'AppBundle\Entity\Benefit',
			'validation_groups' => 'benefit'
		));
	}

	public function getBlockPrefix()
	{
		return 'benefit';
	}

	public function getName() 
	{
        return 'benefit';
    }
}

?>