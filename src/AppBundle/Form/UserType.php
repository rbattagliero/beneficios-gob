<?php

namespace AppBundle\Form;

use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class UserType extends AbstractType
{
	
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('username',TextType::class,array(
				'label' => 'Nombre de usuario'
			))
			->add('email',EmailType::class,array(
				'label' => 'Email'	
			))
			->add('roles',ChoiceType::class,array(
				'choices'  => User::ROLES_OPTIONS,
				'expanded' => true,
				'multiple' => true,
				'required' => false
			))
			->add('firstName',TextType::class,array(
				'label' => 'Nombre'
			))
			->add('lastname',TextType::class,array(
				'label' => 'Apellido'
			))
		;
	}

	public function getParent()
	{
		return 'FOS\UserBundle\Form\Type\RegistrationFormType';
	}
}

?>