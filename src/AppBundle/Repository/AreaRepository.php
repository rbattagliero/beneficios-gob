<?php 

namespace AppBundle\Repository;

class AreaRepository extends \Doctrine\ORM\EntityRepository
{

	public function getBenefitsByFilter($filter = array())
	{
		$query = $this->createQueryBuilder('b')
	      ->where('1 = 1');

	    if(isset($filter['name']) && !empty($filter['name'])){
	        $query->andWhere('b.name LIKE :name')
	        ->setParameter('name', '%'.$filter['name'].'%');
	    }

	    return $query->orderBy('b.name', 'ASC')->getQuery()->getResult();
	}
}
?>