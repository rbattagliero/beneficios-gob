<?php 

namespace AppBundle\Repository;

class BenefitRepository extends \Doctrine\ORM\EntityRepository
{

	public function getBenefitsByFilter($filter = array())
	{
		$query = $this->createQueryBuilder('b')
	      ->where('1 = 1');

	    if(isset($filter['name']) && !empty($filter['name'])){
	        $query->andWhere('b.name LIKE :name')
	        ->setParameter('name', '%'.$filter['name'].'%');
	        $query->orWhere('b.lastname LIKE :lastname')
	        ->setParameter('lastname', '%'.$filter['name'].'%');
	    }

	    if(isset($filter['dni']) && !empty($filter['dni'])){
	        $query->andWhere('b.dni LIKE :dni')
	        ->setParameter('dni', '%'.$filter['dni'].'%');
	    }

	    if(isset($filter['passport']) && !empty($filter['passport'])){
	        $query->andWhere('b.passport LIKE :passport')
	        ->setParameter('passport', '%'.$filter['passport'].'%');
	    }

	    if(isset($filter['cuil']) && !empty($filter['cuil'])){
	        $query->andWhere('b.cuil LIKE :cuil')
	        ->setParameter('cuil', '%'.$filter['cuil'].'%');
	    }

	    if(isset($filter['area']) && !empty($filter['area'])){
	        $query->andWhere('b.area = :area')
	        ->setParameter('area', $filter['area']);
	    }

	    if(isset($filter['ministry']) && !empty($filter['ministry'])){
	        $query->andWhere('b.ministry = :ministry')
	        ->setParameter('ministry', $filter['ministry']);
	    }

	    if(isset($filter['secretariat']) && !empty($filter['secretariat'])){
	        $query->andWhere('b.secretariat = :secretariat')
	        ->setParameter('secretariat', $filter['secretariat']);
	    }

	    if(isset($filter['direction']) && !empty($filter['direction'])){
	        $query->andWhere('b.direction = :direction')
	        ->setParameter('direction', $filter['direction']);
	    }

	    if(isset($filter['type']) && !empty($filter['type'])){
	        $query->andWhere('b.type = :type')
	        ->setParameter('type', $filter['type']);
	    }

	    return $query
	    	->orderBy('b.lastname', 'ASC')
	    	->addOrderBy('b.name', 'ASC')
	    	->getQuery();
	}
}
?>