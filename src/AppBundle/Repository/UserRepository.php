<?php 

namespace AppBundle\Repository;

class UserRepository extends \Doctrine\ORM\EntityRepository
{

	public function getBenefitsByFilter($filter = array())
	{
		$query = $this->createQueryBuilder('b')
	      ->where('1 = 1');

	    if(isset($filter['name']) && !empty($filter['name'])){
	        $query->andWhere('b.firstName LIKE :firstName')
	        ->setParameter('firstName', '%'.$filter['name'].'%');
	        $query->orWhere('b.lastname LIKE :lastname')
	        ->setParameter('lastname', '%'.$filter['name'].'%');
	        $query->orWhere('b.username LIKE :username')
	        ->setParameter('username', '%'.$filter['name'].'%');
	    }

	    return $query->orderBy('b.lastname', 'ASC')->getQuery()->getResult();
	}
}
?>